import React, {Component} from "react";
import '../Button/Style.scss'
class Button extends Component{
    render(){
        const {text,backgroundColor,onClick,className,type} = this.props;
        return(
            <button className={className} type={type} style={{backgroundColor}} onClick={onClick}>{text}</button>
        )
    }
}
Button.defaultProps ={
    type : 'button',
}
export default Button;
