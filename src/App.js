import React, {Component} from 'react';
import './App.scss';
import Button from './components/Button';
import Modal from './components/Modal';

class App extends Component{
  constructor(props){
    super(props)
    this.state = {
      modal1:false,
      modal2:false,
    }
    this.showModalOne = this.showModalOne.bind(this);
    this.showModalTwo = this.showModalTwo.bind(this);
    this.closeModal1 = this.closeModal1.bind(this);
    this.closeModal2 = this.closeModal2.bind(this);
  }
  
  showModalOne() {
    this.setState({modal1: true});
  } 

  showModalTwo() {
    this.setState({modal2: true});
  }
  closeModal1() {
    this.setState({
      modal1: false,
    })
  }
  closeModal2() {
    this.setState({
      modal2: false,
    })
  }

  render(){
    const actionsModal1 =
    <>
      <Button text='ok' onClick={this.closeModal1} className='button_modal'/>
      <Button text='cancel'onClick={this.closeModal1} className='button_modal'/> 
    </>
    const actionsModal2 =
    <>
      <Button text='add' onClick={this.closeModal2} backgroundColor='green' className='button'/>
      <Button text='cancel' onClick={this.closeModal2} backgroundColor='red'className='button'/> 
    </>
    
    return(
      <div className="App">
      <div className='button__wrapper'>
        <Button text='Open first modal'backgroundColor="yellow" className='button' onClick={this.showModalOne} />
        <Button text='Open second modal'backgroundColor="blue" className='button'onClick={this.showModalTwo} />
        {this.state.modal1 &&
        <Modal
        header='Do you want to delete this file?'
        text='Once you delete this file, it wont be posible to undo this action. Are you shure you want to delete this file?'
        btnClose={this.closeModal1}
        actions={actionsModal1}
        />
        }
        {this.state.modal2 &&
        <Modal
        header='Do you want to add this file?'
        text= 'Are you shure you want to add this file?'
        btnClose={this.closeModal2}
        actions={actionsModal2}
        />
        }

      </div>
      
    </div>
    )
  }
}
export default App;

